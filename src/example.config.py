'''
Created on 13 Aug 2017
@author: cave
Copy this file to config.py and update the settings
'''
#!/usr/bin/env python
# encoding: utf-8

'''
Get your API key
Start by retrieving your API Key from the "Security" section in new Account admin panel to be able to make authenticated requests to the API.
https://account.gandi.net/
'''
api_secret = '---my_secret_API_KEY----'

'''
Gandiv5 LiveDNS API Location
http://doc.livedns.gandi.net/#api-endpoint
https://dns.api.gandi.net/api/v5/
'''
api_endpoint = 'https://dns.api.gandi.net/api/v5'

#enter all subdomains to be updated, subdomains must already exist to be updated
#your domain and subdomains to be updated, subdomains must already exist to be updated
dnsentries = {
    "mydomain.tld": {
        "ipv4": ["subdomain1", "subdomain2"],
        "ipv6": ["subdomain3v6"],
        },
    "myotherdomain.tld": {
        "ipv4": ["subdomain4"],
        },
}


#300 seconds = 5 minutes
ttl = '300'

''' 
IP address lookup service 
run your own external IP provider:
+ https://github.com/mpolden/ipd
+ <?php $ip = $_SERVER['REMOTE_ADDR']; ?>
  <?php print $ip; ?>
e.g. 
+ https://api[4|6].ipify.org
+ https://ifconfig.co/ip
+ http://ifconfig.me/ip
+ http://whatismyip.akamai.com/
+ http://ipinfo.io/ip
+ many more ...
'''
ifconfig = 'choose_from_above_or_run_your_own'
ifconfig = {
    "ipv4": 'choose_from_above_or_run_your_own_ipv4_lookup_service',
    "ipv6": 'choose_from_above_or_run_your_own_ipv6_lookup_service',
}
